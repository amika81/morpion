package hellocucumber;
import java.util.Scanner;


public class Main {
    public static  Scanner scan = new Scanner(System.in);
    private static final String  EXIT = "exit";
    private static final String  START = "start" + " (easy|user|medium|hard) +(easy|user|medium|hard)";
    private static final String INPUT_COMND = "Input command: ";

    // Exemple pour lancer : start user hard
    // (uniquement le mode hard utilise l'algorithme minimax)

    public static void main(String[] args) {
        System.out.print(INPUT_COMND);
        String inputCommand = scan.nextLine();

        while (!inputCommand.equals(EXIT)) {
            if (inputCommand.matches(START)) {
                Game game = new Game(inputCommand);
                game.run();
            } else {
                System.out.println("Bad parameters!");
            }

            System.out.print(INPUT_COMND);
            inputCommand = scan.nextLine();
        }

        scan.close();
    }

}
